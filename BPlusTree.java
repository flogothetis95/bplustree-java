import java.util.ArrayList;
import java.util.List;

public class BPlusTree<K extends Comparable<K>, V> {

    private Node root;
    private int degree;

    // Constructor
    public BPlusTree(int degree) {
        this.degree = degree;
        this.root = new LeafNode();
    }
    // Search operation
    public List<V> search(K key) {

        return root.search(key);
    }

    // Insert operation
    public void insert(K key, V value) {
        root.insert(key, value);

        // Check if the root has split
        if (root.isOverflow()) {
            InnerNode newRoot = new InnerNode();
            newRoot.children.add(root);
            root.split(newRoot, 0);
            root = newRoot;
        }
    }

    // Node abstract class
    private abstract class Node {
        List<K> keys;

        Node() {
            keys = new ArrayList<>();
        }

        abstract List<V> search(K key);

        abstract void insert(K key, V value);

        abstract boolean isOverflow();

        abstract void split(InnerNode parent, int childIndex);
    }

    // LeafNode class
    private class LeafNode extends Node {
        List<List<V>> values;
        LeafNode next;

        LeafNode() {
            super();
            values = new ArrayList<>();
            next = null;
        }

        @Override
        List<V> search(K key) {

            int index = findIndex(key);
            if (index != -1 && keys.get(index).equals(key)) {
                return values.get(index);
            }
            return null;
        }

        @Override
        void insert(K key, V value) {
            int index = findIndex(key);
            if(!keys.contains(key)) {
                keys.add(index, key);
                values.add(index, new ArrayList<>());
            }
            values.get(index).add(value);
        }

        @Override
        boolean isOverflow() {
            return keys.size() > degree - 1;
        }

        @Override
        void split(InnerNode parent, int childIndex) {
            LeafNode newNode = new LeafNode();

            int midIndex = keys.size() / 2;
            K newKey = keys.get(midIndex);

            for (int i = midIndex; i < keys.size(); i++) {
                newNode.keys.add(keys.get(i));
                newNode.values.add(values.get(i));
            }

            keys.subList(midIndex, keys.size()).clear();
            values.subList(midIndex, values.size()).clear();

            newNode.next = next;
            next = newNode;

            parent.keys.add(childIndex, newKey);
            parent.children.add(childIndex + 1, newNode);
        }

        private int findIndex(K key) {
            int index = 0;
            while (index < keys.size() && keys.get(index).compareTo(key) < 0) {
                index++;
            }
            return index;
        }
    }

    // InnerNode class
    private class InnerNode extends Node {
        List<Node> children;

        InnerNode() {
            super();
            children = new ArrayList<>();
        }

        @Override
        List<V> search(K key) {
            int index = findIndex(key);
            return children.get(index).search(key);
        }

        @Override
        void insert(K key, V value) {
            int index = findIndex(key);
            Node child = children.get(index);
            child.insert(key, value);

            if (child.isOverflow()) {
                child.split(this, index);
            }
        }

        @Override
        boolean isOverflow() {
            return keys.size() > degree;
        }

        @Override
        void split(InnerNode parent, int childIndex) {
            InnerNode newNode = new InnerNode();

            int midIndex = (keys.size() - 1) / 2;
            K newKey = keys.get(midIndex);

            for (int i = midIndex + 1; i < keys.size(); i++) {
                newNode.keys.add(keys.get(i));
                newNode.children.add(children.get(i));
            }

            newNode.children.add(children.get(keys.size()));
            children.subList(midIndex + 1, keys.size() + 1).clear();
            keys.subList(midIndex, keys.size()).clear();

            parent.keys.add(childIndex, newKey);
            parent.children.add(childIndex + 1, newNode);
        }

        private int findIndex(K key) {
            int index = 0;
            while (index < keys.size() && keys.get(index).compareTo(key) <= 0) {
                index++;
            }
            return index;
        }
    }
}